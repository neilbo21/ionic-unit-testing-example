import { Page } from './app.po';

describe('App', () => {
  let page: Page;

  beforeEach(() => {
    page = new Page();
  });

  describe('default screen', () => {

    it("should absorb the race condition in Bitbucket Pipelines", () => {
      page.navigateTo("/").then(() => {
        console.info("Page loaded");
          page.getPageTitleText("page-page1").then(title => {
            expect(title).toEqual("Page One");
          });
      }).catch((err) => {
        console.error("Page NOT loaded: ", err);
        console.log("####################################################");
        console.log("You are seeing this because:");
        console.log("1. In the current CI/CD tool:");
        console.log("   there is a race condition on the 1st E2E spec.");
        console.log("2. Protractor runs before Angular is ready");
        console.log("   even though it shouldn\'t.");
        console.log("####################################################");
      })
    });

    it('should have a title saying Page One', () => {
      page.navigateTo("/").then(() => {
          page.getPageTitleText("page-page1").then(title => {
            expect(title).toEqual('Page One');
          });
      });
    });
  });
});
