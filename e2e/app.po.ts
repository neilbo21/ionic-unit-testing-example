import { browser, by, element, protractor } from 'protractor';

export class Page {

  navigateTo(destination) {
    return browser.get(destination);
  }

  getTitle() {
    return browser.getTitle();
  }

  getPageTitleText(selector) {
    let title = element(by.tagName(selector)).element(by.tagName('ion-title')).element(by.css('.toolbar-title'))
    return title.getText();
  }
}
