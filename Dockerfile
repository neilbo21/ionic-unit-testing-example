FROM node:8.11.1

LABEL version="1.0.1"
LABEL maintainter="neilbo"
LABEL npm="6.1.0"

###########################
# Install 
###########################

RUN npm install -g npm@6.1.0
RUN npm install -g ionic cordova
RUN apt-get update
RUN apt-get install vim nano -y
RUN apt-get install default-jre -y
RUN apt-get install libxss1 libappindicator1 libindicator7 -y
RUN apt-get install fonts-liberation libatk-bridge2.0-0 -y libgtk-3-0 lsb-release xdg-utils -y
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update
RUN apt-get install google-chrome-stable -y
RUN apt-get update
RUN node -v
RUN npm -v